
- Student List:
   * URL: /api/v1/student
   * Method: GET
   * Request: {domain}/api/v1/student?limit=10&page=1&column=id&sort=asc
   * Response:
      Sucess:
            {
                    "status": true,
                    "code": 200,
                    "students": [
                        {
                            "id": 1,
                            "name": "Phan Minh Cường",
                            "email": "minhcuong@gmail.com",
                            "phone": "0974743376",
                            "created_at": "2020-11-09T04:30:02.000000Z",
                            "updated_at": "2020-11-09T04:30:02.000000Z"
                        },
                        {
                            "id": 3,
                            "name": "Ngọc Trinh",
                            "email": "ngoctrinh@gmail.com",
                            "phone": "09686868668",
                            "created_at": "2020-11-09T05:16:19.000000Z",
                            "updated_at": "2020-11-09T05:16:19.000000Z"
                        },
                        {
                            "id": 4,
                            "name": "Chipu",
                            "email": "chipu@gmail.com",
                            "phone": "0988888999",
                            "created_at": "2020-11-09T05:50:00.000000Z",
                            "updated_at": "2020-11-09T08:27:38.000000Z"
                        }
                    ],
                    "meta": {
                        "total": 14,
                        "perpage": "3",
                        "currentPage": 1
                    }
            }
            -----------------------------------------------------
            -----------------------------------------------------
        fail:
            {
                "status": false,
                "code": 500,
                "message": "Undefined variable: studentss"
            }



- Create Student:
   * URL: /api/v1/student
   * Method: POST
   * Request:
            {
                name: "sample name",
                email: "sample email",
                phone: "sample phone"
            }
   * Response:
        Sucess:
                {
                    "status": true,
                    "code": 200,
                    "student": {
                        "name": "Phuong Anh",
                        "email": "phuonganh@gmail.com",
                        "phone": "0929292929"
                    }
                }
        --------------------------------------------
        --------------------------------------------
        Fail:
                 {
                    "status": false,
                    "code": 500,
                    "message": "Undefined property: App\\Http\\Controllers\\Api\\StudentController::$studentServicse"
                 }

- Update An Existing Student:
   * URL: /api/v1/student/{id}
   * Method: PUT
   * Request:
            {
                name: "sample name",
                email: "sample email",
                phone: "sample phone"
            }
   * Response:
        Sucess:
                {
                    "status": true,
                    "code": 200,
                    "student": {
                        "name": "Phuong Anh",
                        "email": "phuonganh@gmail.com",
                        "phone": "0929292929"
                    }
                }
        --------------------------------------------
        --------------------------------------------
        Fail:
                 {
                    "status": false,
                    "code": 500,
                    "message": "Undefined property: App\\Http\\Controllers\\Api\\StudentController::$studentServicse"
                 }

- Show A Specific Student:
   * URL: /api/v1/student/{id}
   * Method: GET
   * Request:
   * Response:
        Sucess:
                {
                    "status": true,
                    "code": 200,
                    "student": {
                        "name": "Phuong Anh",
                        "email": "phuonganh@gmail.com",
                        "phone": "0929292929"
                    }
                }
        --------------------------------------------
        --------------------------------------------
        Fail:
                 {
                    "status": false,
                    "code": 500,
                    "message": "Undefined property: App\\Http\\Controllers\\Api\\StudentController::$studentServicse"
                 }

- Delete A Student:
   * URL: /api/v1/student/{id}
   * Method: DELETE
   * Request:
   * Response:
        Sucess:
                {
                    "status": true,
                    "code": 200
                }
        --------------------------------------------
        --------------------------------------------
        Fail:
                 {
                    "status": false,
                    "code": 500,
                    "message": "Undefined property: App\\Http\\Controllers\\Api\\StudentController::$studentServicse"
                 }


