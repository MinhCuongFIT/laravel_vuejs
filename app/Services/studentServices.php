<?php
namespace App\Services;
use App\Student;

class studentServices{

    public function save(array $data, int $id = null){
        $student = Student::find($id);
        if (!$student) {
            return Student::insert([
            [
                'name' => $data['name'],
                'email' => $data['email'],
                'phone' => $data['phone']
            ],

        ]);
        }else{
            return Student::where('id', $id)
            ->update([
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'phone' => $data['phone']
            ]);
        
        }

    }
    public function findById(int $id){
        return Student::find($id);
    }
    public function deleteById($id = []){
        return Student::destroy($id);
    }
    public function getAll($orderBy = [], $limit = 10){
        $query = Student::query();
        if($orderBy){
            $query->orderBy($orderBy['column'], $orderBy['sort']);
        }
        return $query->paginate($limit);
    }
}