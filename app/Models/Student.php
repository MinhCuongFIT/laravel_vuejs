<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use School;

class Student extends Model
{
    use Notifiable;
      protected $fillable = [
        'name', 'email', 'phone'
    ];

    public function school(){
         return $this->belongsTo(School::class, 'student_id', 'id');
    }
}
