<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    //protected $primaryKey = 'school_id';
    protected $fillable = [
        'school_name', 'student_id'
    ];
    public function student(){
        return $this->hasMany(Student::class,'id','student_id');
    }
    public function teachers(){
        return $this->belongsToMany(Teacher::class);
    }
}