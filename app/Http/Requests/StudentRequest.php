<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  => 'required|min:5|max:40',
            'email' => 'required|email|unique:students',
            'phone' => 'required|min:9|max:10'
        ];
    }
    public function messages()
    {
        return [
            'name.required'  => "Name is required!",
            'email.required' => "Email is required!",
            'phone.required' => "Phone number is required!"
        ];
    }
}