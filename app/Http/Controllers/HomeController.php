<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\School;
use App\Models\Student;
use App\Models\Teacher;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // $schools = School::with('student')->get();
        // dd($schools);
        // $student = Student::with('school')->get();
        // dd($student);

        $teacher = Teacher::with('schools')->get();
        dd($teacher);
        return view('home');
    }
}
