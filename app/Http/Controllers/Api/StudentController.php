<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Student;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Services\studentServices;
use App\Http\Requests\StudentRequest;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $studentService;
    public function __construct(studentServices $studentService)
    {
        $this->studentService = $studentService;
    }

    public function index(Request $request)
    {
        try {
            $limit = $request->get('limit') ?? config('app.paginate.per_page');
            $orderBy = [];
            if ($request->get('column') && $request->get('sort')) {
                $orderBy['column'] = $request->get('column');
                $orderBy['sort'] = $request->get('sort');
            }

            $students = $this->studentService->getAll($orderBy, $limit);
            return response()->json([
                'status'   => true,
                'code'     => Response::HTTP_OK,
                'students' => $students->items(),
                'meta'     => [
                    'total'        => $students->total(),
                    'perpage'      => $students->perPage(),
                    'currentPage'  => $students->currentPage()
                ]
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'code' => Response::HTTP_INTERNAL_SERVER_ERROR,
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StudentRequest $request)
    {
        // $validated = $request->validate();
        // echo $validated;

        try {
            $student =  $this->studentService->save([
                'name' => $request->name,
                'email' => $request->email,
                'phone' => $request->phone
            ]);
            return response()->json([
                'status'   => true,
                'code'     => Response::HTTP_OK,
                'student'  => [
                    'name' => $request->name,
                    'email' => $request->email,
                    'phone' => $request->phone
                ]
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'code' => Response::HTTP_INTERNAL_SERVER_ERROR,
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $student =  $this->studentService->findById($id);
            return response()->json([
                'status'   => true,
                'code'     => Response::HTTP_OK,
                'student'  => $student
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'code' => Response::HTTP_INTERNAL_SERVER_ERROR,
                'message' => $e->getMessage()
            ]);
        }
    }

    public function update(StudentRequest $request, $id)
    {
        try {
            $student =  $this->studentService->save([
                'name' => $request->name,
                'email' => $request->email,
                'phone' => $request->phone
            ], $id);
            return response()->json([
                'status'   => true,
                'code'     => Response::HTTP_OK,
                'student'  => [
                    'name' => $request->name,
                    'email' => $request->email,
                    'phone' => $request->phone
                ]
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'code' => Response::HTTP_INTERNAL_SERVER_ERROR,
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->studentService->deleteById([$id]);
            return response()->json([
                'status'   => true,
                'code'     => Response::HTTP_OK,
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'code' => Response::HTTP_INTERNAL_SERVER_ERROR,
                'message' => $e->getMessage()
            ]);
        }
    }
}