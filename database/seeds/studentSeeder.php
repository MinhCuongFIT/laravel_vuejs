<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class studentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        for($i=0; $i<10;$i++){
           DB::table('students')->insert([
                'name' => Str::random(10),
                'email' => Str::random(10).'@gmail.com',
                'phone' => Str::random(10),
           ]);
        }

    }
}