<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;

class teacherSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array = [0,1];
         for($i=0; $i<10;$i++){
           DB::table('teachers')->insert([
                'teacher_name' => Str::random(10),
                'teacher_email' => Str::random(10).'@gmail.com',
                'teacher_address' => Str::random(10),
                'teacher_gender' => Arr::random($array),
           ]);
        }
    }
}