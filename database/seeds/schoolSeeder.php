<?php

use Illuminate\Database\Seeder;
use App\Models\School;

class schoolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 10; $i++) {
            $schools = School::insert(
                [
                    'school_name' => 'School ' .$i,
                    'student_id' => $i
                ]
            );
        }
    }
}